## MRM 
## Le Monde D'Après

====================================
## SETUP
To see the project, follow these steps:

**1 Download Composer dependencies**
After unzipping the project, open a terminal and run:
```
composer install
```

**2 Configure the .env.local file**
Open the `.env` file and make any adjustments you need - specifically
`DATABASE_URL`. Or, if you want, you can create a `.env.local` file
and *override* any configuration you need there (instead of changing
`.env` directly).

**3: Download the Webpack Encore Assets**
This app uses Webpack Encore.
To install it and get it running, use the following command:
```
npm install
```

**4 Setup the Database**
Again, make sure `.env` is setup for your computer. Then, create
the database & tables with the following commands:
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```
If you have problems, completely drop the
database (`doctrine:database:drop --force`) and try again.

**5 Start the built-in web server**
To start the web server, open a terminal, move into the project directory, and run:
```
symfony serve or php -S localhost:8080 -t public/
```
Alternatively, you can add -d after the serve command to run the server as a background daemon, and still be able to use the terminal window.

**6 Access the server on your browser**
You're now able to check out the site at `https://localhost:8080`


====================================
## Principe
-Appli open-source collaborative a but non lucratif, permettant de voter pour ou contre des entreprises, afin de leur attribuer des aides ou des sanctions d'état.

## Fonctionnalités
-Citoyens peuvent s'enregistrer.
-Les citoyens peuvent s'identifier grace a un identifiant unique
-Les citoyens peuvent alors s'informer par le biais d'articles et de statistiques sur les différentes entreprises.
-Après s'être informés, ils peuvent décider quelle entreprise mérite leur vote.
-Entreprises peuvent être nominées dans différentes catégories (responsabilité: environnement, social; poids: économique: secteur, emplois).
-Chaque catégorie aurait podium: premiers, derniers.
-Les meilleurs se verraient attribuer des aides de l'état, les derniers des sanctions, spécifiques à chaque catégories.

## Accès utilisateurs
-User arrive: accès a page d'acceuil, statistiques et articles, liste d'entreprises, résultats des votes, commentaires, page d'enregistrement (email de confirmation)/login
-User crée son compte avec numéro unique: accès au système de vote, peut poster des commentaires (images, photos)
-Admin: Système de vérification des numéros d'utilisateurs, vérification des données des entreprises

-Inscription: NOM, PRENOM, upload de photocopie de carte d'identité/sociale (BROUILLON: code unique a 10 chiffres) afin de vérifier que le code correspond, permet le vote.
-Après inscription, envoi de mail précisant que la demande va etre vérifiée et traitée, uniquement APRES CONFIRMATION DU MAIL
-Vote: 1 vote par an

## Schéma de données
https://trello-attachments.s3.amazonaws.com/5ec2a365a4c7680f15e827bf/5ec2a3cdc65dd928b1a081bc/edb66d1cef29e24137530e77b7157f2b/Capture_d%E2%80%99%C3%A9cran_de_2020-05-19_16-46-28.png


===============================

Brief projet "Le monde d'après" 
===============================

Suite à la crise du coronavirus, nous souhaitons vous proposer le développement d'une plateforme permettant aux citoyens du monde entier d'imaginer le monde d'après. Notre système est à bout de souffle, l'hyper optimisation de celui-ci le rend fragile, quelles sont les modifications à apporter pour le rendre plus résilient ?

L'objectif principal est de proposer une plateforme collaborative offrant aux internautes un moyen d'imaginer, de débattre, de fédérer,... pour la construction d'un nouveau modèle de société. Une idée simple peut amener de grands changements, devenez acteurs de votre futur !

## Objectifs
* Prise en main plus approfondie de Symfony
* Identifier problématique(s) et proposer une solution 
* Imaginer et concevoir une application
* Travailler en groupe de façon efficace
* Gestion de projet (tâches, planning,..)

## Compétences visées
* Compétence 1, maquettage : niveau transposer
* Compétence 2, web statique : niveau transposer
* Compétence 5, créer une base de donnée : niveau transposer
* Compétence 6, développer les composants d'accès aux données : niveau adapter/transposer
* Compétence 7, développer la partie back-end d'une application : niveau transposer

## Modalités pédagogiques
Ce projet se déroulera sur 4 semaines (18 jours) en 2 sprints, le travail sera réalisé par groupe de 3 personnes.

## Etapes
1. Définir un projet
2. Réalisation de wireframes
3. Définir les différentes tâches dans un **Trello**
4. Traiter les tâches définies

## Tâches

### Sprint 1 : jusqu'au 29 mai
Vous l'aurez compris le sujet étant totalement libre, vous serez en charge de définir les tâches de votre projet. Néanmoins quelques fonctionnalités devront **obligatoirement être implémentées** :

* Upload de fichiers
* Envoie de mails,... (sms ?)
* Composant React alimentée par du JSON

> Attention, l'objectif de ce projet n'est pas de faire du javascript mais simplement de réaliser le lien entre une application React et Symfony. Il ne faudra pas passer trop de temps sur l'application React, pour se faire nous vous proposons simplement l'implémentation d'[autocomplete de material-ui](https://material-ui.com/components/autocomplete/)


### Sprint 2 :
Chronologie : le sprint 2 se déroulera du 2 au 12 juin 2020

# Objectifs
* Etre en capacité d'avoir une application finalisée avec toutes les étapes nécessaires à une application de qualité
* Les problèmes de sécurité sont connus et les préconisations minimales sont mises en place
* L'application est prête à l'emploi et utilisable par un tiers (readme, traduction, tests)

# Compétences visées
* Compétence 6, développer les composants d'accès aux données : niveau transposer
* Compétence 7, développer la partie back-end d'une application : niveau transposer

# Les tâches
Les points suivants devront être intégrés au projet :

* Readme (install) : dead line  mercredi 3/06/20 (avec fixtures)
* Intégrer les validateurs pour tous les champs de formulaire
* Gestion de plusieurs rôles
* Intégrer les traductions
* Test

## Readme
L'objectif est d'avoir un readme accessible sur la branche master de votre git. N'importe quelle personne en suivant les instructions doit être en capacité d'installer votre projet, de lancer les fixtures et d'avoir une application qui fonctionne.

## Test
Les modules à tester devront être identifié. Au moins un test unitaire doit être implémenté, l'idéal est que tous les points critiques identifiés soient testés.

# Les étapes
## Etape 1
Ré-organiser son Trello pour finaliser les tâches demandées dans le sprint 1

## Etape 2
Réaliser les tâches du Trello

## Etape 3
12 juin 13h30 : rendu du projet "le monde d'après"