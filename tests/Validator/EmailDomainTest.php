<?php

namespace App\Tests\Validator;

use App\Validator\EmailDomain;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Exception\MissingOptionsException;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class EmailDomainTest extends TestCase
{
    
    public function test_requiredparameters()
    {
        $this->expectException(MissingOptionsException::class);
        new EmailDomain();
    }

    public function test_badshapedblockedparameter()
    {
        $this->expectException(ConstraintDefinitionException::class);
        new EmailDomain(["blocked" => "azertyuiop"]);
    }

    public function test_optionissetasproperty()
    {
        $arr = ["a", "b", "c"];
        $domain = new EmailDomain(["blocked" => $arr]);
        $this->assertEquals($arr,$domain->blocked);
    }

}