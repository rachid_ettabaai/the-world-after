<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserEntityTest extends KernelTestCase
{
    private function getUserEntity(): User
    {
        $user = new User();
        $user->setEmail("testuser@gmail.com");
        $user->setPassword("testpwduser");
        $user->setFirstName("Jean");
        $user->setLastName("Louis");
        $user->setPersonalCode("1234678965");
        $user->setPhone("0678987656");
        $user->setAddress("3 Rue de la Vie");
        $user->setCity("Lyon");
        $user->setDocument("img.png");
        $user->setImageFile(null);

        return $user;
    }

    private function assetHasErrors(int $nberrors,User $user)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($user);
        $this->assertCount($nberrors, $error);

    }

    public function test_validUserEntity()
    {
        $user = $this->getUserEntity();
        $this->assetHasErrors(0,$user);
    }

    public function test_invalidUserEntity()
    {
        $user = $this->getUserEntity()->setPersonalCode("1234678")->setPassword("test");
        $this->assetHasErrors(2, $user);
    }
}