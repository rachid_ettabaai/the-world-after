import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import ApiCategory from './ApiCompagny'

 function App() {

     return (
         <div className="col-10 ">
             <ApiCategory/>
         </div>
     )

}

ReactDOM.render(<App />, document.querySelector('#app'));

// let url = "https://127.0.0.1:8000/api/categories";
// async function fetchAPI() {
//     const response = await fetch(url);
//     const json  =  await response.json();
//     return json;
// }
// async function getCategory(){
//
//     const fetchApi = await fetchAPI();
//
//     const categorys = fetchApi["hydra:member"];
//     return categorys;
// }
//
// getCategory().then(data =>{
//     console.log(data);
// })

