<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 * @Vich\Uploadable
 * @ApiResource(
 *     normalizationContext={"groups"={"company:comp"}}
 * )
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("company:comp")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $webSite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $picture;

    /**
     * @ORM\ManyToMany(targetEntity=NominationYear::class, mappedBy="company")
     */
    private $nominationYears;

    /**
     * @ORM\OneToMany(targetEntity=Votes::class, mappedBy="company")
     */
    private $votes;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("company:comp")
     */
    private $name;

    /**
     * @var string|null
     * 
     * @ORM\Column(name="document", type="string", length=255)
     */
    private $document;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="companys", fileNameProperty="document")
     */
    private $imageFile;

    /**
     * @var \DateTimeInterface|null
     * 
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if ($this->imageFile instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function __construct()
    {
        $this->nominationYears = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->updated_at = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function setWebSite(string $webSite): self
    {
        $this->webSite = $webSite;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return Collection|NominationYear[]
     */
    public function getNominationYears(): Collection
    {
        return $this->nominationYears;
    }

    public function addNominationYear(NominationYear $nominationYear): self
    {
        if (!$this->nominationYears->contains($nominationYear)) {
            $this->nominationYears[] = $nominationYear;
            $nominationYear->addCompany($this);
        }

        return $this;
    }

    public function removeNominationYear(NominationYear $nominationYear): self
    {
        if ($this->nominationYears->contains($nominationYear)) {
            $this->nominationYears->removeElement($nominationYear);
            $nominationYear->removeCompany($this);
        }

        return $this;
    }

    /**
     * @return Collection|Votes[]
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    public function addVote(Votes $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes[] = $vote;
            $vote->setCompany($this);
        }

        return $this;
    }

    public function removeVote(Votes $vote): self
    {
        if ($this->votes->contains($vote)) {
            $this->votes->removeElement($vote);
            // set the owning side to null (unless already changed)
            if ($vote->getCompany() === $this) {
                $vote->setCompany(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocument(): ?string
    {
        return $this->document;
    }

    /**
     * @param string $document
     */
    public function setDocument(?string $document): void
    {
        $this->document = $document;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
