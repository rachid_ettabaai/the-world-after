<?php

namespace App\Controller;

use App\Repository\NominationYearRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/",name="home")
     */
    public function index(NominationYearRepository $nominationYearRepository)
    {
        //dd($nominationYearRepository->findAll());
        return $this->render("anonymous/index.html.twig", [
            'nominationsYears' => $nominationYearRepository->findAll()
        ]);
    }

    /**
     * @Route("/entrepises",name="home_companys")
     */
    public function viewCompanys()
    {
        return $this->render("anonymous/companys.html.twig");
    }
}