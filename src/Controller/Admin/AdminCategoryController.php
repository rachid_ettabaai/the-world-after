<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/categories",name="admin_categories_")
 */
class AdminCategoryController extends AbstractController
{
    private $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * @Route("/",name="list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $categories = $this->category->findAll();
        return $this->render("admin/category/categories.html.twig",compact("categories"));
    }

    /**
     * Form processing for the creation of a new customer
     * @param FormInterface $form
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function processform(FormInterface $form, Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        if (is_null($category->getId())) {
            $em->persist($category);
            $this->addFlash("success", "Catégorie créée avec succès");
        } else {
            $em->persist($form->getData());
            $this->addFlash("success", "Catégorie modifiée avec succès");
        }
        $em->flush();

        return $this->redirectToRoute("admin_categories_list");
    }

    /**
     * @Route("/create",name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $category);
        }
        $forms = $form->createView();
        return $this->render("admin/category/categorycreate.html.twig", compact("category", "forms"));
    }

    /**
     * @Route("/edit/{id}",name="edit")
     * 
     * @param Request $request
     * @param Category $category
     * @IsGranted("MANAGE", subject="category")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Category $category, Request $request)
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $category);
        }
        $forms = $form->createView();
        return $this->render("admin/category/categoryedit.html.twig", compact("category", "forms"));
    }

    /**
     * @Route("/delete/{id}",name="delete")
     * @param Request $request
     * @param Category $category
     * @IsGranted("MANAGE", subject="category")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Category $category, Request $request)
    {
        if ($this->isCsrfTokenValid("delete" . $category->getId(), $request->get("_token"))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }
        return $this->redirectToRoute("admin_categories_list");
    }

}