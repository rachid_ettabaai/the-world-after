<?php

namespace App\Controller\User;

use App\Entity\Company;
use App\Entity\NominationYear;
use App\Entity\Votes;
use App\Repository\NominationYearRepository;
use App\Repository\UserRepository;
use App\Repository\VotesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity as TakeId;


/**
 * Class UserHomeController
 * @package App\Controller\User
 * @Route("/user",name="user_")
 */
class UserHomeController extends AbstractController
{
    private $user;


    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @Route("/",name="home")
     * @param Request $request
     * @param NominationYearRepository $nominationYearRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, NominationYearRepository $nominationYearRepository): Response
    {
        $user = $this->user->findOneBy(['email' => $request->getSession()->get("email")]);

        $nominationsYears = $nominationYearRepository->findAll();
        //dump($user);
        return $this->render("user/userIndex.html.twig",compact("user","nominationsYears"));

    }

    /**
     * @Route("/nominationYear/votes",name="ny_vote")
     * @param NominationYearRepository $nominationYearRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNominationYearVote(NominationYearRepository $nominationYearRepository)
    {
        return $this->render("user/userNominationYearVotes.html.twig",[
            'NominationYear' => $nominationYearRepository->findAll(),
            'curentValue' => 'VotesUser'
        ]);
    }


    /**
     * @Route("/nominationYear/{idNy}/company",name="company")
     * @TakeId("nominationYear", expr="repository.find(idNy)")
     * @param NominationYear $nominationYear
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ChoiceCompagnyForVote( NominationYear $nominationYear): \Symfony\Component\HttpFoundation\Response
    {


        return $this->render("user/userCompany.html.twig",
            [
                'companys' => $nominationYear->getCompany(),'idNy' => $nominationYear,
                'curentValue' => 'VotesUser'

            ]);
    }

    /**
     * @Route("/nominationYear/{idNy}/compagny/{idComp}/",name="vote")
     * @TakeId("company", expr="repository.find(idComp)")
     * @TakeId("nominationYear", expr="repository.find(idNy)")
     * @param Company $company
     * @param NominationYear $nominationYear
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function VoteForCompagnyInCategorie(Request $request, Company $company, NominationYear $nominationYear, EntityManagerInterface $manager)
    {

        $user = $this->user->findOneBy(['email' => $request->getSession()->get("email")]);
        $vote = new Votes();
        $vote->setDate(new \DateTime('now'));
        $vote->setNominationYear($nominationYear);
        $vote->setCompany($company);
        $vote->setUser($user);
        $manager->persist($vote);
        $manager->flush();
        $this->addFlash("vote", "Votre vote a été pris en compte merci.");
        return $this->redirectToRoute('user_ny_vote');
    }

    /**
     * @Route("/vues_des_votes/", name="view_vote")
     * @param VotesRepository $votesRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewVotesUser(Request $request, VotesRepository $votesRepository): Response
    {

        $user = $this->user->findOneBy(['email' => $request->getSession()->get("email")])->getId();
        $votesUser = $votesRepository->findBy(['user' => $user]);
        //dd($votesUser);
        return $this->render("user/userViewVotes.html.twig",
            [
           'votesUser' => $votesUser,
           'curentValue' => 'VotesViews'
            ]);

    }
    /**
     * @Route("/nominationYear/view",name="ny_view")
     * @param NominationYearRepository $nominationYearRepository
     * @return Response
     */
    public function getNominationYearViews(NominationYearRepository $nominationYearRepository)
    {
        return $this->render("user/userNominationYearViews.html.twig",[
            'NominationYear' => $nominationYearRepository->findAll(),
            'curentValue' => 'NominationYearViews'
        ]);
    }



}
