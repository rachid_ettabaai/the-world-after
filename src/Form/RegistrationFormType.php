<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',EmailType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Email([
                    'message' => 'Invalid email address',
                    ])
                ]
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Password',
                'mapped' => false,
                'constraints' => [
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('firstName',TextType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a firstName',
                    ]),
                    new Length(['min' => 3]),
                    new Regex([
                        'pattern' => '/\d/',
                        'match' => false,
                        'message' => 'Your firstname cannot contain a number',
                    ])
                ]
            ])
            ->add('lastName',TextType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a lastName',
                    ]),
                    new Length(['min' => 3]),
                    new Regex([
                        'pattern' => '/\d/',
                        'match' => false,
                        'message' => 'Your lastname cannot contain a number',
                    ])
                ]
            ])
            ->add('personalCode',NumberType::class,[
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(0|[1-9][0-9]*)$/',
                        'match' => true,
                        'message' => 'Your personal code cannot contain a letter',
                    ])
                ]
            ])
            ->add('phone',NumberType::class, [
            'constraints' => [
                new Regex([
                    'pattern' => '/^[0-9\-\+]{9,15}$/',
                    'match' => true,
                    'message' => 'Your phone number does not have the expected format',
                ])
            ]
        ])
            ->add('address',TextType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter an address',
                    ])
                ]
            ])
            ->add('city', TextType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a city',
                    ])
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/png',
                            'image/jpx',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image file',
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
