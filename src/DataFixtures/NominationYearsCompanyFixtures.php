<?php

namespace App\DataFixtures;

use App\Repository\CompanyRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Repository\NominationYearRepository;
use Doctrine\Common\Persistence\ObjectManager;

class NominationYearsCompanyFixtures extends Fixture
{
    private $nominationyears;
    private $companies;
    
    public function __construct(NominationYearRepository $nominationyears, 
                                CompanyRepository $companies)
    {
        $this->nominationyears = $nominationyears;
        $this->companies = $companies;
    }

    public function load(ObjectManager $manager)
    {
        $nbnominationyears = $this->nominationyears->count([]);
        $nbcompanies = $this->companies->count([]);

        for ($i=1; $i < 20; $i++) { 
            
            $nominationyear = $this->nominationyears->findOneBy(["id" => mt_rand(1,$nbnominationyears)]);
            $company = $this->companies->findOneBy(["id" => mt_rand(1, $nbcompanies)]);
            $nominationyear->addCompany($company);
            $company->addNominationYear($nominationyear);
            $manager->persist($nominationyear);
            $manager->persist($company);

        }

        $manager->flush();

    }

}