<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Company;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Bezhanov\Faker\ProviderCollectionHelper;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CompanyFixtures extends Fixture
{
    private $faker;

    public function __construct()
    {
       $this->faker = Factory::create("fr_FR");
    }

    private function uploadimagefromurl(Company $company,string $url)
    {
        $imagefilename = new Slugify();
        $contents = file_get_contents($url);
        $filename = $imagefilename->slugify($this->faker->company) . ".png";
        $company->setDocument($filename);
        $company->setPicture($filename);
        $dirnamedirectory = dirname(\dirname(__DIR__)) . "/public/images/";

        if (file_exists($dirnamedirectory)) {
            $directory = $dirnamedirectory . "companys/";
        } else {
            mkdir($dirnamedirectory);
            $directory = $dirnamedirectory . "companys/";
        }

        if (file_exists($directory)) {
            $file = $directory . $filename;
        } else {
            mkdir($directory);
            $file = $directory . $filename;
        }

        file_put_contents($file, $contents);
        $imagefile = new UploadedFile($file, $filename);
    }

    public function load(ObjectManager $manager)
    {
        ProviderCollectionHelper::addAllProvidersTo($this->faker);

        for ($i=1; $i < 20; $i++) { 
            
            $company = new Company();
            $company->setDescription($this->faker->sentence);
            $company->setAddress($this->faker->address);
            $company->setZipCode($this->faker->postcode);
            $company->setCity($this->faker->city);
            $company->setCountry($this->faker->country);
            $company->setWebSite($this->faker->url);
            $company->setName($this->faker->company);
            
            $urlimg = "https://fakeimg.pl/350x200/?text=company".$i;
            $this->uploadimagefromurl($company, $urlimg);
            $company->setImageFile(null);
            $manager->persist($company);
            
        }
        $manager->flush();
    }
}